﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class rotateslide : MonoBehaviour {

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ValueChanged()
    {

        GameObject catapult = GameObject.Find("Catapult");
        Slider slider = GetComponent<Slider>();
        GameObject cube = GameObject.Find("Cube");
        GameObject ammo = GameObject.Find("Ammo");

        cube.transform.parent = catapult.transform;
        ammo.transform.parent = catapult.transform;

        catapult.transform.rotation = Quaternion.Euler(new Vector3(0, slider.value, 0));

        cube.transform.parent = null;
        ammo.transform.parent = null;



    }
}
