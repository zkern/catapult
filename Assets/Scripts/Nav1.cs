﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Nav1 : MonoBehaviour
{

    private Vector3[] points = new Vector3[]
    {
        new Vector3(3.0f, 0, 0),
        new Vector3(3.0f, 0, 3.0f),
        new Vector3(0, 0, 0)
    };

    private Vector3 originalPosition;
    private NavMeshAgent agent;
    private int destPoint = 0;

    // Use this for initialization
    void Start()
    {
        originalPosition = gameObject.transform.position;

        agent = GetComponent<NavMeshAgent>();

        GotoNextPoint();
    }

    // Update is called once per frame
    void Update()
    {
        if (!agent.pathPending && agent.remainingDistance < 0.5f)
            GotoNextPoint();
    }

    void GotoNextPoint()
    {
        if (points.Length == 0)
            return;

        agent.destination = points[destPoint] + originalPosition;

        destPoint = (destPoint + 1) % points.Length;
    }
}
