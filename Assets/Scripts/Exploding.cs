﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Exploding : MonoBehaviour
{



    public static int Score = 0;
    public Text s;
    public GameObject explosion; // drag your explosion prefab here

    void start()
    {

        Score = 0;
        setScore();
    }


    private void setScore()
    {
        s.text = "Score " + Score;

    }



void OnCollisionEnter(Collision col)
{

    if (col.gameObject.tag == "CanDestroy")
    {
          

        GameObject expl = Instantiate(explosion, transform.position, Quaternion.identity) as GameObject;
       
        Destroy(expl, 3); 
    
            Destroy(col.gameObject);
            Score++;
            setScore();



    }
}
}
	

