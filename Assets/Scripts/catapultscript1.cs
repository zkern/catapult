﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class catapultscript1 : MonoBehaviour
{

    private Vector3 catapultPosition;
    private Quaternion catapultRotation;
    private Vector3 ammoPosition;
    private Quaternion ammoRotation;
    public int timeLeft = 60;
    public Text countdownText;
    public Text gameOver;
    private static int Score;
   


    void Start()
    {
        Physics.autoSimulation = false;
        StartCoroutine("LoseTime");

    }

    void setScore()
    {

    }


    void Update()
    {
        countdownText.text = ("Time Left " + timeLeft);

        if (timeLeft <= 0)
        {

            StopCoroutine("LoseTime");
            Score = Exploding.Score;
            countdownText.text = " You Are Out of Time";
            gameOver.text ="Your score is " + Score;
        }
    }

    private void OnMouseDown()
    {
        GameObject catapult = GameObject.Find("Catapult");
        GameObject ammo = GameObject.Find("Ammo");
        if (timeLeft > 0)
        {
            if (!Physics.autoSimulation)
            {
                Physics.autoSimulation = true;
                catapultPosition = catapult.transform.position;
                ammoPosition = ammo.transform.position;
                catapultRotation = catapult.transform.rotation;
                ammoRotation = ammo.transform.rotation;
            }
            else
            {
                Physics.autoSimulation = false;
                catapult.transform.position = catapultPosition;
                ammo.transform.position = ammoPosition;
                catapult.transform.rotation = catapultRotation;
                ammo.transform.rotation = ammoRotation;
            }
        }

    }
    IEnumerator LoseTime()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            timeLeft--;

        }
    }
}
