﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;
public class Navigation1 : MonoBehaviour
{

    public Transform Target;

    void Start()
    {

        NavMeshAgent agent = GetComponent<NavMeshAgent>();
        agent.destination = Target.position;
    }

}
