﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class forceslider1 : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ValueChanged()
    {
        Slider slider = GetComponent<Slider>();

        GameObject catapult = GameObject.Find("Catapult");
        HingeJoint joint = catapult.GetComponent<HingeJoint>();

        JointSpring spring = joint.spring;
        spring.spring = slider.value;
        joint.spring = spring;

    }
}
